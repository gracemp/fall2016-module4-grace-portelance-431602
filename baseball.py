import sys, os
import re
from collections import OrderedDict
#create a class to track stats
class Stat():
    def __init__ (self, player):
        self.player = player
        self.hits = 0
        self.attempts = 0
    
    def upToBat(self, attempts):
        self.attempts += attempts
    
    def addHit(self, hits):
        self.hits += hits
        
    def getHits(self):
        return self.hits
        
    def addAttempt(self, attempts):
        self.attempts += attempts
        
    def batAvg(self):
        if int(self.attempts != 0):
			 return round(float(self.hits) /float(self.attempts), 3)
        else:
			 return 0
# initialize two dictionaries, one for holding one for printing
playerDict = {}
printable = {}
#check if file exists and is readable
if len(sys.argv) < 2:
	sys.exit("Usage: %s filename" % sys.argv[0]) 
filename = sys.argv[1]

if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])
f = open(filename)
#read file
for line in f:
    player_regex = re.compile(r"(?P<name>(\b[A-Z]([\w])* \b){2})")
    m = player_regex.match(line)
    if m != None:
        attempts_regex = re.search(r'(?:\S+\s)(?=times)', line)
        hits_regex = re.search(r'(?:\S+\s)(?=hits)', line)
        n = m.group(1)
        if n in playerDict:
            py = playerDict[n]
            py.addHit(float(hits_regex.group(0)))
            py.upToBat(float(attempts_regex.group(0)))
            printable[n] = py.batAvg()
        else:
		#initialize player
            p = Stat(n)
            p.addHit(float(hits_regex.group(0)))
            p.upToBat(float(attempts_regex.group(0)))
            playerDict[n] = p

sorted_x = OrderedDict(sorted(printable.items(), key=lambda x: x[1], reverse = True))
# Loop through the dictionary using orderedDict
for k, v in sorted_x.items():
    print "%s: %s" % (k, v)

